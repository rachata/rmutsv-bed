package com.ipcamera.demo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public interface api {

    @FormUrlEncoded
    @POST("/rmutsv/status.php")
    Call<status> log(@Field("status") int log);
}
